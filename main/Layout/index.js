import React from 'react'
import { View } from 'react-native'
import { observer } from 'startupjs'
import './index.styl'

export default observer(function ({ children }) {
  return pug`
    View.root
      = children
  `
})
