import React from 'react'
import { View, Text } from 'react-native'
import { observer } from 'startupjs'
import { faRocket } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import './index.styl'

export default observer(function Header ({ style }) {
  return pug`
    View.root(style=style)
      View.logo
        FontAwesomeIcon(
          icon=faRocket
          color='#8cc9f0'
          size=28
        )
      Text.title Stellar
      View.wrapper
        Text.description Just another free, fully responsive site template built by&nbsp;
          Text.link @nikin94
          Text.description  for 
          Text.link Dmapper
          Text.description .
  `
})
