import React from 'react'
import { View } from 'react-native'
import { observer } from 'startupjs'
import Info from './Info'
import Contacts from './Contacts'
import Socials from './Socials'
import Copyright from './Copyright'
import './index.styl'

export default observer(function Footer ({ style }) {
  return pug`
    View.root(style=style)
      View.wrapper
        Info.info
        View.right
          Contacts
          Socials
      Copyright
  `
})
