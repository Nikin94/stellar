import React from 'react'
import { View, Text } from 'react-native'
import { observer } from 'startupjs'
import './index.styl'

export default observer(function Copyright ({ style }) {
  return pug`
    View.root(style=style)
      Text.description © Untitled. Design:&nbsp;
        Text.description(styleName=['link']) HTML5 UP
        Text.description . Demo Images:&nbsp;
        Text.description(styleName=['link']) Picsum
        Text.description .
  `
})
