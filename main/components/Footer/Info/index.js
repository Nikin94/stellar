import React from 'react'
import { View, Text } from 'react-native'
import { observer } from 'startupjs'
import { Button, Title } from 'components'
import meta from './meta.json'
import './index.styl'

export default observer(function Info ({ style }) {
  return pug`
    View.root(style=style)
      Title.title(
        text=meta.title
        color='#fff'
        separator=false
      )
      Text.text= meta.text
      Button.button(textColor='#fff')
  `
})
