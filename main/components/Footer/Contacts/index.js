import React from 'react'
import { View, Text } from 'react-native'
import { observer, emit } from 'startupjs'
import { Title } from 'components'
import meta from './meta.json'
import './index.styl'

export default observer(function Contacts ({ style }) {
  return pug`
    View.root(style=style)
      Title.title(
        text=meta.title
        color='#fff'
        separator=false
      )
      View.contacts
        each contact, index in meta.contacts
          View.contact(key=index align='between' styleName={first: index === 0})
            Text.contactTitle= contact.label
            if contact.link
              Text.value(
                styleName=['link']
                onPress=() => emit('url', contact.link)
              )= contact.value
            else
              Text.value= contact.value
  `
})
