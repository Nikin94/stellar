import React from 'react'
import { View, TouchableOpacity } from 'react-native'
import { observer } from 'startupjs'
import {
  faTwitter,
  faFacebook,
  faInstagram,
  faGithub,
  faDribbble
} from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import './index.styl'

export default observer(function Socials ({ style }) {
  const icons = [faTwitter, faFacebook, faInstagram, faGithub, faDribbble]
  return pug`
    View.root(style=style)
      each social, index in icons
        TouchableOpacity.social(key=index styleName={first: index === 0})
          FontAwesomeIcon.icon(icon=icons[index])
  `
})
