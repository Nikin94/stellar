import React, { useState } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { observer } from 'startupjs'
import './index.styl'

export default observer(function Nav ({ style, labels }) {
  const [selectedLabel, setSelectedLabel] = useState(labels[0])
  return pug`
    View.root(style=style)
      each label, index in labels
        TouchableOpacity.item(
          key=index
          styleName={
            first: index === 0,
            selected: label === selectedLabel
          }
          onPress=() => setSelectedLabel(label)
        )
          Text.label= label
  `
})
