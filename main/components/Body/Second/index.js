import React from 'react'
import { View, Text } from 'react-native'
import { observer } from 'startupjs'
import { Button, Title } from 'components'
import {
  faCodeBranch,
  faFolderOpen,
  faSignal,
  faLaptop,
  faGem
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import meta from './meta.json'
import './index.styl'

export default observer(function Second ({ style }) {
  const icons = [faCodeBranch, faFolderOpen, faSignal, faLaptop, faGem]

  return pug`
    View.root(style=style)
      Title(text=meta.title)
      Text.description= meta.description
      View.cards
        each card, index in meta.cards
          View.card(
            key=index
            styleName={
              first: index === 0,
              last: index === meta.cards.length - 1
            }
            style={ backgroundColor: card.color }
          )
            FontAwesomeIcon.icon(icon=icons[index])
            Text.cardTitle= card.title
            Text.cardText= card.text
      Text.text= meta.text
      Button.btn
  `
})
