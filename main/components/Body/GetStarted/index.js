import React from 'react'
import { View, Text } from 'react-native'
import { observer } from 'startupjs'
import { Title, Button } from 'components'
import meta from './meta.json'
import './index.styl'

export default observer(function GetStarted ({ style }) {
  return pug`
    View.root(style=style)
      Title(text=meta.title)
      Text.text= meta.text
      View.actions
        Button.getStarted(
          label='Get started'
          textColor='#fff'
        )
        Button.learn
  `
})
