import React from 'react'
import { View, Text, Image } from 'react-native'
import { observer } from 'startupjs'
import { Button, Title } from 'components'
import meta from './meta.json'
import './index.styl'

export default observer(function Introduction ({ style }) {
  return pug`
    View.root(style=style)
      View.imageWrapper
        Image.image(source={uri: meta.url })
      View.content
        Title.title(text=meta.title)
        Text.text= meta.text
        Button.btn
  `
})
