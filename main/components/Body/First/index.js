import React from 'react'
import { View, Text, Image } from 'react-native'
import { observer } from 'startupjs'
import { Title, Button } from 'components'
import meta from './meta.json'
import './index.styl'

export default observer(function First ({ style }) {
  return pug`
    View.root(style=style)
      Title(text=meta.title)
      View.cards
        each card, index in meta.cards
          View.card(key=index styleName={first: index === 0})
            View.imageWrapper
              Image.image(source={uri: card.url })
            Text.cardTitle= card.title
            Text.cardText= card.text
      Button.btn
  `
})
