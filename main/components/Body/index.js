import React, { useMemo } from 'react'
import { View } from 'react-native'
import { observer } from 'startupjs'

import Section from './Section'
import Nav from './Nav'
import Introduction from './Introduction'
import First from './First'
import Second from './Second'
import GetStarted from './GetStarted'

import './index.styl'

export default observer(function Body ({ style }) {
  const sections = [
    {
      value: Introduction,
      label: 'Introduction'
    },
    {
      value: First,
      label: 'First section'
    },
    {
      value: Second,
      label: 'Second section'
    },
    {
      value: GetStarted,
      label: 'Get started'
    }
  ]

  const labels = useMemo(() => sections.map(s => s.label), [JSON.stringify(sections)])

  const getComponent = item => {
    const Component = item
    return pug`Component`
  }

  return pug`
    View.root(style=style)
      Nav.nav(labels=labels)
      each section, index in sections
        Section(key=index)
          =getComponent(section.value)
  `
})
