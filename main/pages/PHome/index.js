import React from 'react'
import { View, ScrollView } from 'react-native'
import { observer } from 'startupjs'
import { Header, Body, Footer } from 'main/components'
import './index.styl'

export default observer(function PHome () {
  return pug`
    ScrollView.root(contentContainerStyle={alignItems: 'center'})
      View.content
        Header
        Body
        Footer
  `
})
