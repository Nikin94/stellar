import React from 'react'
import { View, Text } from 'react-native'
import { observer } from 'startupjs'
import { Separator } from 'components'
import './index.styl'

export default observer(function Title ({
  style,
  text = 'Title',
  color,
  separator = true
}) {
  const textStyle = {}
  if (color) textStyle.color = color
  return pug`
    View.root(style=style)
      Text.text(style=textStyle)= text
      if separator
        Separator
  `
})
