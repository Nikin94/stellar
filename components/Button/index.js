import React from 'react'
import { Text, TouchableOpacity } from 'react-native'
import { observer, emit } from 'startupjs'
import './index.styl'

export default observer(function Button ({
  style,
  onPress = () => emit('url', '/'),
  label = 'Learn More',
  textColor,
  ...props
}) {
  const textStyles = {}
  if (textColor) textStyles.color = textColor
  return pug`
    TouchableOpacity.root(style=style onPress=onPress ...props)
      Text.text(style=textStyles)= label
  `
})
