import TestThing from './TestModel'

export default function (racer) {
  racer.orm('testThings.*', TestThing)
}
